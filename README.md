### - Clone the project:
git clone https://gitlab.com/rahmatilla/mailingmanagement.git

### - Enter project repository:
cd mailingManagement

### - Create virtual environment:
python -m venv venv

### - Activate virtual environment:
Windows: venv\Scripts\activate.bat  
Linux: source venv/bin/activate

### - Install requirements:
pip install -r requirements.txt

### - Run Makemigrations and Migrate commands:
python manage.py makemigrations\
python manage.py migrate 

### - Create super user:
python manage.py createsuperuser

### - Run project:
python manage.py runserver

### - API documentation url:
http://127.0.0.1:8000/docs/

### - Admin page url:
http://127.0.0.1:8000/admin/


