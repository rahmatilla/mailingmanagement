from django.apps import AppConfig


class SmsmailingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'smsmailing'

    # def ready(self):
    #     from .schedule import start
    #     start()
