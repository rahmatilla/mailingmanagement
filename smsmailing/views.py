from rest_framework import generics, permissions
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Mailing, Client, Message
from .serializers import MailingSerializer, ClientSerializer, MessageSerializer
from datetime import datetime
from pytz import timezone
import requests


class ClientView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientRetrieve(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class MailingView(generics.ListCreateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MailingRetrieve(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

class MessageView(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

class MailingSummaryView(APIView):

    def get(self, request):
        query_mailing_count = Mailing.objects.all().count()
        query_message_success_count = Message.objects.filter(status=0).count()
        query_message_failed_count = Message.objects.filter(status=-1).count()

        return Response({'total_mailing': query_mailing_count,
                         'total_success_message': query_message_success_count,
                         'total_failed_message': query_message_failed_count})

class MailingSummaryRetrieve(APIView):
    def get(self, request, pk):
        mailing_obj = get_object_or_404(Mailing, pk=pk)
        query_message_success_count = Message.objects.filter(status=0, mailing_id=mailing_obj.id).count()
        query_message_failed_count = Message.objects.filter(status=-1, mailing_id=mailing_obj.id).count()

        return Response({'mailing_id': mailing_obj.id,
                         'success_message': query_message_success_count,
                         'failed_message': query_message_failed_count})

def sendSMS(msgId, phone, text):
    auth_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTQ4MzU5NjEsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9SYWtobWF0aWxsYV84OSJ9.1iZlPMBPIofjrJ422oAf1-8yq7dqxn4IFMfjgWUXCFs'
    headers = {'Authorization': f'Bearer {auth_token}'}
    data = {
        "id": msgId,
        "phone": phone,
        "text": text
    }
    url = f'https://probe.fbrq.cloud/v1/send/{msgId}'

    try:
        response = requests.post(url, json=data, headers=headers)
    except:
        return -1
    return response.json()['code']

def mailingSMS():
    current_time = datetime.now(timezone('UTC'))
    print(current_time)
    mailings = Mailing.objects.filter(is_done=False, start_time__lte=current_time, end_time__gte=current_time)
    print(mailings)
    if mailings is not None:
        for mailing in mailings:
            client_lst = mailing.clientList()
            for client in client_lst:
                new_message = Message()
                new_message.mailing_id = mailing.id
                new_message.client_id = client.id
                new_message.save()
                msg_id = new_message.id
                msg_status = sendSMS(msg_id, client.phone_number, mailing.text_message)
                Message.objects.filter(pk=msg_id).update(status=msg_status)
            mailing.is_done = True
            mailing.save()





