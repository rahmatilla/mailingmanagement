from django.contrib import admin
from .models import Client, Mailing, Message

class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone_number', 'mnc', 'tag', 'timezone')

admin.site.register(Client, ClientAdmin)

class MailingAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_time', 'text_message', 'receivers_type', 'end_time')

admin.site.register(Mailing, MailingAdmin)

class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_time', 'status', 'mailing_id', 'client_id')

admin.site.register(Message, MessageAdmin)
