from apscheduler.schedulers.background import BackgroundScheduler
from .views import mailingSMS

def start():
	scheduler = BackgroundScheduler()
	scheduler.add_job(mailingSMS, 'interval', seconds=30)
	scheduler.start()