from django.db import models
from .client import Client
from .mailing import Mailing

STATUS = (
    (-1, 'Failed'),
    (0, 'OK')

)

class Message(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, choices=STATUS, null=True)
    mailing_id = models.IntegerField()
    client_id = models.IntegerField()