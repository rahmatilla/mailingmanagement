from django.db import models
from .client import Client


class Mailing(models.Model):
    start_time = models.DateTimeField()
    text_message = models.CharField(max_length=200)
    receivers_type = models.ForeignKey(Client, db_column='tag', on_delete=models.PROTECT)
    end_time = models.DateTimeField()
    is_done = models.BooleanField(default=False)

    def clientList(self):
        return Client.objects.filter(tag=self.receivers_type)






































































