from django.db import models

class Client(models.Model):
    phone_number = models.CharField(max_length=11)
    mnc = models.CharField(max_length=3)
    tag = models.CharField(max_length=20)
    timezone = models.CharField(max_length=10)

    def __str__(self):
        return self.tag

