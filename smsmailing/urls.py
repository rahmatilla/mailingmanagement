from django.urls import path
from .views import ClientView, ClientRetrieve, MailingView, MailingRetrieve, MessageView, MailingSummaryView, MailingSummaryRetrieve


urlpatterns = [
    path('client/', ClientView.as_view()),
    path('client/<int:pk>/', ClientRetrieve.as_view()),
    path('mailing/', MailingView.as_view()),
    path('mailing/<int:pk>/', MailingRetrieve.as_view()),
    path('message/', MessageView.as_view()),
    path('mailing/summary/', MailingSummaryView.as_view()),
    path('mailing/summary/<int:pk>/', MailingSummaryRetrieve.as_view())
]