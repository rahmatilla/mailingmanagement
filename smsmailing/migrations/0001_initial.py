# Generated by Django 4.2.1 on 2023-05-08 10:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone_number', models.CharField(max_length=11)),
                ('mnc', models.CharField(max_length=3)),
                ('tag', models.CharField(max_length=20)),
                ('timezone', models.CharField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Mailing',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.DateTimeField()),
                ('text_message', models.CharField(max_length=200)),
                ('end_time', models.DateTimeField()),
                ('receivers_type', models.ForeignKey(db_column='tag', on_delete=django.db.models.deletion.PROTECT, to='smsmailing.client')),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(choices=[(0, 'Failed'), (1, 'Successful')], max_length=20)),
                ('client_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='smsmailing.client')),
                ('mailing_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='smsmailing.mailing')),
            ],
        ),
    ]
